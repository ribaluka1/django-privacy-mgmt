from django.contrib.sites.models import Site
from django.db import models
try:
    from django.utils.translation import ugettext_lazy as _
except ImportError:
    from django.utils.translation import gettext_lazy as _


TRACKING_ITEM_CHOICE_ESSENTIALS = 'ESSENTIAL'
TRACKING_ITEM_CHOICE_STATISTICS = 'STATISTICS'
TRACKING_ITEM_CHOICE_MARKETING = 'MARKETING'
TRACKING_ITEM_CHOICE_SOCIAL_NETWORKS = 'SOCIAL_NETWORKS'
TRACKING_ITEM_CHOICE_PERSONALIZATION = 'PERSONALIZATION'

TRACKING_ITEM_CHOICES = (
    (TRACKING_ITEM_CHOICE_ESSENTIALS, _('Essential')),
    (TRACKING_ITEM_CHOICE_STATISTICS, _('Statistics')),
    (TRACKING_ITEM_CHOICE_MARKETING, _('Marketing')),
    (TRACKING_ITEM_CHOICE_SOCIAL_NETWORKS, _('Social Networks')),
    (TRACKING_ITEM_CHOICE_PERSONALIZATION, _('Personalization')),
)


class TrackingItem(models.Model):
    name = models.CharField(_('Name'), max_length=1024)

    category = models.CharField(
        default=TRACKING_ITEM_CHOICE_ESSENTIALS,
        choices=TRACKING_ITEM_CHOICES,
        max_length=1024,
        null=False,
        blank=False,
    )

    ordering = models.IntegerField(_('Ordering'), default=0)
    # needs to be optional for x-site entries
    site = models.ManyToManyField(
        Site,
        blank=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('ordering',)
