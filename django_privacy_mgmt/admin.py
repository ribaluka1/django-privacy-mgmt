from django.contrib import admin
from modeltranslation.admin import TabbedTranslationAdmin

from .models import TrackingItem

@admin.register(TrackingItem)
class TrackingItemAdmin(TabbedTranslationAdmin):
    list_display = ('id', 'name', 'category', )
    search_fields = ('name', )
    list_filter = ('category', )
    list_display_links = ('id', 'name')

    filter_vertical = ('site', )

    def has_module_permission(self, request):
        return request.user.is_superuser
