from modeltranslation.translator import translator, TranslationOptions
from .models import TrackingItem


class TrackingItemTranslation(TranslationOptions):
    fields = ('name',)
translator.register(TrackingItem, TrackingItemTranslation)
